<?php

namespace Drupal\bfm\Commands;

use Drupal\bfm\Batch\BfmCopyBatchInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drush\Drupal\Commands\core\MessengerCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Bfm Drush commands handler.
 */
class BfmCommands extends MessengerCommands {

  use StringTranslationTrait;

  /**
   * The file_system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The bfm.files_copy_batch service.
   *
   * @var \Drupal\bfm\Batch\BfmCopyBatchInterface
   */
  protected BfmCopyBatchInterface $copyBatch;

  /**
   * BfmCommands constructor.
   */
  public function __construct(MessengerInterface $messenger, FileSystemInterface $file_system, BfmCopyBatchInterface $copy_batch) {
    parent::__construct($messenger);
    $this->fileSystem = $file_system;
    $this->copyBatch = $copy_batch;
  }

  /**
   * Recursively copy files from one directory to another.
   *
   * @command bfm:copy
   * @option source Required: The source directory to copy files from.
   * @option destination Required: The destination directory to copy the files to.
   * @option condition Limits when to migrate files one of always,newer,size,newer_size. Default always.
   * @usage drush bfm:copy --source=/var/www/html/web/sites/files/ --destination='newScheme://'
   *   Bulk copy files from one directory to another..
   */
  public function copyFiles(
    $options = [
      'source' => '',
      'destination' => '',
      'condition' => 'always',
    ]
  ): void {

    $source = $options['source'];
    if (!is_dir($source) || !is_readable($source) || !is_executable($source)) {
      $this->logger()->error(new TranslatableMarkup('Source is not a readable directory.'));
      return;
    }

    $destination = $options['destination'];
    if (empty($destination)) {
      throw new \Exception('Destination must not be empty');
    }
    $dir_exist = $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);
    if (!$dir_exist) {
      throw new \Exception('Destination is not a directory');
    }

    $upload_options = [];
    switch ($options['condition']) {
      case 'always':
        $upload_options['upload_conditions']['always'] = TRUE;
        break;

      case 'newer_size':
        $upload_options['upload_conditions']['newer'] = TRUE;
        $upload_options['upload_conditions']['size'] = TRUE;
        break;

      case 'newer':
        $upload_options['upload_conditions']['newer'] = TRUE;
        break;

      case 'size':
        $upload_options['upload_conditions']['size'] = TRUE;
        break;
    }

    $this->logger()->notice(
      new TranslatableMarkup(
        'You are going to copy files from @source to @destination.',
        [
          '@source' => $source,
          '@destination' => $destination,
        ]
      )
    );

    if (!$this->io()->confirm(new TranslatableMarkup('Are you sure?'))) {
      throw new UserAbortException();
    }

    $batch = $this->copyBatch->createBatch($source, $destination, $upload_options);

    if (!empty($batch)) {
      batch_set($batch);
      drush_backend_batch_process();
    }

  }

}
