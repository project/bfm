<?php

namespace Drupal\bfm\Batch;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Service for file copy batches.
 *
 *  Copy files from a source to destination directory.
 */
final class BfmFilesCopyBatch implements BfmCopyBatchInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Construct the BfmFilesCopyBatch service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string_translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(TranslationInterface $string_translation, MessengerInterface $messenger) {
    $this->setStringTranslation($string_translation);
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public function createBatch(string $source, string $destination, array $upload_options): ?array {

    // We use a DirectoryIterator instead of
    // FileSystemInterface::scanDirectory() for performance on large datasets.
    $directory_iterator = new \RecursiveDirectoryIterator(
      $source,
      \FilesystemIterator::SKIP_DOTS
    );
    $files = new \RecursiveIteratorIterator($directory_iterator);

    try {
      $total = iterator_count($files);
    }
    catch (\UnexpectedValueException $e) {
      $this->messenger()->addError(
        $this->t(
          'Unable to read all directories, %error',
          ['%error' => $e->getMessage()]
        )
      );
      return NULL;
    }

    if ($total == 0) {
      $this->messenger()->addMessage($this->t("No files to migrate."), 'ok');
      return NULL;
    }

    // Create batch.
    $batch = [
      'finished' => [get_class($this), 'finished'],
      'title' => $this->t('Copy files'),
      'init_message' => $this->t('The copying process is about to start..'),
      'progress_message' => $this->t('Processed batch @current out of @total.'),
      'error_message' => $this->t('Something wrong happened, please check the logs.'),
    ];

    $file_batch_chunk = [];

    /** @var \SplFileInfo $file */
    foreach ($files as $file) {
      $file_batch_chunk[] = $file->getPathName();
      if (count($file_batch_chunk) == 50) {
        $batch['operations'][] = [
          [
            get_class($this),
            'copyOperation',
          ],
          [
            $file_batch_chunk,
            $source,
            $destination,
            $upload_options,
            $total,
          ],
        ];

        $file_batch_chunk = [];
      }
    }

    if (count($file_batch_chunk) !== 0) {
      $batch['operations'][] = [
        [
          get_class($this),
          'copyOperation',
        ],
        [
          $file_batch_chunk,
          $source,
          $destination,
          $upload_options,
          $total,
        ],
      ];
    }
    return $batch;

  }

  /**
   * {@inheritdoc}
   */
  public static function copyOperation(array $file_paths, string $src_path_base, string $destination, array $upload_options, int $total, array|\DrushBatchContext &$context) {

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');

    // Ensure $destination ends with '/'.
    if (mb_substr($destination, -1) !== '/') {
      $destination .= '/';
    }

    if (!isset($context['results']['src_path'])) {
      $context['results']['src_path'] = $src_path_base;
      $context['results']['destination_path'] = $destination;
      $context['results']['progress'] = 0;
      $context['results']['percent_progress'] = 0;
      $context['results']['total'] = $total;
      $context['results']['time_start'] = time();
      $context['results']['errors'] = [];
    }

    $src_strip_len = strlen(rtrim($src_path_base, '/')) + 1;
    foreach ($file_paths as $source_file) {

      $relative_path = substr_replace($source_file, '', 0, $src_strip_len);
      $destination_filename = $destination . $relative_path;

      $upload_conditions = [];
      if (isset($upload_options['upload_conditions'])) {
        $upload_conditions = $upload_options['upload_conditions'];
      }

      if (static::isFileAlreadyCopied($source_file, $destination_filename, $upload_conditions)) {
        self::updateProgress($context);
        continue;
      }

      $directory = $file_system->dirname($destination_filename);
      if (!$file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
        $context['results']['errors'][] = new TranslatableMarkup(
          'Failed to create directory @directory',
          ['@directory' => $directory]
        );
        self::updateProgress($context);
        continue;
      }

      try {
        $file_system->copy($source_file, $destination_filename, FileSystemInterface::EXISTS_REPLACE);
      }
      catch (FileException) {
        $context['results']['errors'][] = new TranslatableMarkup(
          'Failed to copy @file',
          ['@file' => $source_file]
        );
      }
      self::updateProgress($context);
    }
  }

  /**
   * Copy Operation progress message generator.
   *
   * @param array|\DrushBatchContext $context
   *   Batch context.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Markup containing progress message.
   */
  private static function getCopyOperationMessage(array|\DrushBatchContext $context) {
    return new TranslatableMarkup('@percent_progress% (@progress/@total) time elapsed @elapsed_time (hh:mm:ss)', [
      '@percent_progress' => $context['results']['percent_progress'],
      '@progress' => $context['results']['progress'],
      '@total' => $context['results']['total'],
      '@elapsed_time' => static::getElapsedTimeFormatted($context['results']['time_start']),
    ]);
  }

  /**
   * Formatted string of time elapsed on batch process.
   *
   * @param int $time_start
   *   Time batch started as seconds since unix epoch.
   *
   * @return string
   *   Zero padded elapsed time in "hh:mm:ss" format.
   */
  private static function getElapsedTimeFormatted($time_start) {
    $time_elapsed_seconds = time() - $time_start;
    return gmdate('H:i:s', $time_elapsed_seconds);
  }

  /**
   * {@inheritdoc}
   */
  public static function finished($success, array $results, array $operations) {

    $msgText = new TranslatableMarkup(
      'Copied files from %source to %destination in  @elapsed_time (hh:mm:ss).',
      [
        '%source' => $results['src_path'],
        '%destination' => $results['destination_path'],
        '@elapsed_time' => static::getElapsedTimeFormatted($results['time_start']),
      ]
    );

    if (!empty($results['errors'])) {
      $msgText .= '<br>' . new TranslatableMarkup("The following errors occurred:");
      foreach ($results['errors'] as $error) {
        $msgText .= '<br>' . $error;
      }
    }

    \Drupal::messenger()->addStatus(new FormattableMarkup($msgText, []));
  }

  /**
   * Updates the progress counter and display.
   *
   * @param array|\DrushBatchContext $context
   *   Batch context passed by reference.
   */
  private static function updateProgress(array|\DrushBatchContext &$context) {
    // Update our progress information.
    $context['results']['progress']++;

    // Show status message each 5 files.
    if ($context['results']['progress'] % 5 == 0) {
      $current_percent_progress = floor(($context['results']['progress'] / $context['results']['total']) * 100);

      if ($context['results']['percent_progress'] != $current_percent_progress) {
        $context['results']['percent_progress'] = $current_percent_progress;
      }

      $context['message'] = static::getCopyOperationMessage($context);
    }
  }

  /**
   * Determine if a file has already been copied.
   *
   * @param string $source_uri
   *   The uri/path of source file.
   * @param string $dest_uri
   *   The uri/path of destination file.
   * @param array $upload_conditions
   *   An array of upload conditions. Currently accepts the following keys:
   *     - newer -- IF the dest is newer the file will not be copied.
   *     - size -- If sizes are equal the file will not be copied.
   *   If keys are TRUE those conditions will be used to evalue if destination
   *   is the same file as source.
   *
   * @return bool
   *   TRUE if the file is already copied, else FALSE.
   */
  private static function isFileAlreadyCopied(string $source_uri, string $dest_uri, array $upload_conditions = []): bool {
    $source_stat = @stat($source_uri);
    $dest_stat = @stat($dest_uri);

    // Source has been deleted since batch created.
    if (empty($source_stat)) {
      return TRUE;
    }

    // No conditions or dest doesnt exist.
    if (empty($upload_conditions) || empty($dest_stat)) {
      return FALSE;
    }

    if (!empty($upload_conditions['newer'])) {
      if ($source_stat['mtime'] > $dest_stat['mtime']) {
        return FALSE;
      }
    }

    if (!empty($upload_conditions['size'])) {
      if ($source_stat['size'] != $dest_stat['size']) {
        return FALSE;
      }
    }

    return TRUE;

  }

}
