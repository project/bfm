<?php

namespace Drupal\bfm\Batch;

/**
 * Interface for bfm File Copy Batch .
 *
 *  Copy files from one path to another.
 */
interface BfmCopyBatchInterface {

  /**
   * Creat a batch that willc copy files from one path to another.
   *
   * @param string $source
   *   Source location to copy from.
   * @param string $destination
   *   Destination of copied files.
   * @param array $upload_options
   *   Options to control upload operations.
   *
   * @return array|null
   *   An array of batch data to be provided to batch_set().
   */
  public function createBatch(string $source, string $destination, array $upload_options): ?array;

  /**
   * Batch operation callback that will copy files.
   *
   * @param array $file_paths
   *   Array with file paths to process.
   * @param string $src_path_base
   *   Base path of files being copied. Will be removed when determining
   *   the final destination filename.
   * @param string $destination
   *   Folder to copy the files to.
   * @param array $upload_options
   *   Options to control upload operations.
   * @param int $total
   *   Total number of files to process in batch.
   * @param array|\DrushBatchContext $context
   *   Batch context.
   */
  public static function copyOperation(array $file_paths, string $src_path_base, string $destination, array $upload_options, int $total, array|\DrushBatchContext &$context);

  /**
   * Finished batch callback.
   *
   * @param bool $success
   *   Whether the batch completed successfully or not.
   * @param array $results
   *   The results key of the batch context.
   * @param array $operations
   *   The operations that were carried out.
   */
  public static function finished($success, array $results, array $operations);

}
