<?php

namespace Drupal\bfm\Form;

use Drupal\bfm\Batch\BfmCopyBatchInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an actions form.
 */
class BfmCopyFiles extends FormBase {

  /**
   * The file_system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The files_copy_batch service.
   *
   * @var \Drupal\bfm\Batch\BfmCopyBatchInterface
   */
  protected BfmCopyBatchInterface $bfmCopyBatch;

  /**
   * Construct a new BfmCopyFiles form instance.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string_translation service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   Drupal file_system service.
   * @param \Drupal\bfm\Batch\BfmCopyBatchInterface $files_copy_batch
   *   The Files Copy Batch service.
   */
  public function __construct(TranslationInterface $string_translation, FileSystemInterface $file_system, BfmCopyBatchInterface $files_copy_batch) {
    $this->setStringTranslation($string_translation);
    $this->fileSystem = $file_system;
    $this->bfmCopyBatch = $files_copy_batch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation'),
      $container->get('file_system'),
      $container->get('bfm.files_copy_batch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bfm_copy_files';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['copy_params'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Copy files'),
    ];

    $form['copy_params']['upload_condition'] = [
      '#type' => 'select',
      '#title' => $this->t('Upload Condition'),
      '#description' => $this->t(
        "When to upload files"
      ),
      '#options' => [
        'always' => $this->t('Always'),
        'newer' => $this->t('Modified time is newer'),
        'size' => $this->t('File size differs'),
        'newer_size' => $this->t('Modified time or file size'),
      ],
      '#default_value' => 'always',
      '#required' => TRUE,
    ];

    $form['copy_params']['source'] = [
      '#type' => 'textfield',
      '#title' => "Source",
    ];

    $form['copy_params']['destination'] = [
      '#type' => 'textfield',
      '#title' => "Destination",
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#prefix' => '<br>',
      '#name' => 'copy',
      '#value' => $this->t('Copy Files'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $source = $form_state->getValue('source');
    if (!is_dir($source) || !is_readable($source) || !is_executable($source)) {
      $form_state->setErrorByName(
        'source',
        $this->t('Source is not a readable directory')
      );
    }

    $destination = $form_state->getValue('destination');
    $dir_exist = $this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);
    if (!$dir_exist) {
      $form_state->setErrorByName(
        'destination',
        $this->t('Destination is not a directory')
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $submitted_condition = $form_state->getValue('upload_condition');
    $upload_conditions = [];
    switch ($submitted_condition) {
      case 'always':
        $upload_conditions['always'] = TRUE;
        break;

      case 'newer_size':
        $upload_conditions['newer'] = TRUE;
        $upload_conditions['size'] = TRUE;
        break;

      case 'newer':
        $upload_conditions['newer'] = TRUE;
        break;

      case 'size':
        $upload_conditions['size'] = TRUE;
        break;
    }

    $upload_options = [
      'upload_conditions' => $upload_conditions,
    ];

    $source = $form_state->getValue('source');
    $destination = $form_state->getValue('destination');

    $batch = $this->bfmCopyBatch->createBatch($source, $destination, $upload_options);
    if ($batch) {
      batch_set($batch);
    }

  }

}
