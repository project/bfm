<?php

namespace Drupal\Tests\bfm\Kernel;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Bfm Files Copy Batch Test.
 *
 * Ensure that the drush command executes when called..
 *
 * @group bfm
 *
 * @covers \Drupal\bfm\Batch\BfmFilesCopyBatch
 */
class BfmFilesCopyBatchTest extends KernelTestBase {

  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'bfm',
    'system',
  ];

  /**
   * Run a full batch without any mocking.
   */
  public function testBatchRun() {
    $this->installSchema('system', ['sequences']);

    $global_batch = &batch_get();
    $messenger = \Drupal::messenger();
    $source_dir = 'public://copy_source/';
    $dest_dir = 'public://copy_destination/';

    mkdir($source_dir);

    $test_files_binary = $this->getTestFiles('binary');

    $files_to_be_copied = [];
    // Make sure we copy binary files.
    /** @var \stdClass $file */
    foreach ($test_files_binary as $file) {
      $source_uri = mb_ereg_replace('public://', $source_dir, $file->uri);
      $dest_uri = mb_ereg_replace('public://', $dest_dir, $file->uri);
      $files_to_be_copied[$source_uri] = $dest_uri;
      copy($file->uri, $source_uri);
    }

    $copy_batch_service = \Drupal::service('bfm.files_copy_batch');

    $batch = $copy_batch_service->createBatch($source_dir, $dest_dir, []);
    batch_set($batch);
    $this->assertEmpty($messenger->messagesByType(MessengerInterface::TYPE_STATUS), 'No status messages generated during batch');
    $global_batch['progressive'] = FALSE;
    batch_process();
    // Count is 2 higher because of scandir() returning '.' and '..' entries.
    $this->assertCount(count($files_to_be_copied) + 2, scandir($dest_dir), 'Correct count of files copied');
    $this->assertCount(1, $messenger->messagesByType(MessengerInterface::TYPE_STATUS), 'One status messages generated during batch');
    $this->assertStringContainsString('Copied files', $messenger->messagesByType(MessengerInterface::TYPE_STATUS)[0], "Success message sent to messenger included");
    $this->assertStringNotContainsString('errors', $messenger->messagesByType(MessengerInterface::TYPE_STATUS)[0], 'No errors reported');
  }

}
