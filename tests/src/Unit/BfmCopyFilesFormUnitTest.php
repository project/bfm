<?php

namespace Drupal\Tests\bfm\Unit;

use Drupal\bfm\Batch\BfmCopyBatchInterface;
use Drupal\bfm\Form\BfmCopyFiles;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormState;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\Constraint\TraversableContainsEqual;

/**
 * Bfm Drush Test.
 *
 * Ensure that the drush command executes when called..
 *
 * @group bfm
 *
 * @covers \Drupal\bfm\Form\BfmCopyFiles
 */
class BfmCopyFilesFormUnitTest extends UnitTestCase {

  /**
   * Coverage test for building form.
   */
  public function testBuildForm() {

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $bfm_copy_batch_mock = $this->createMock(BfmCopyBatchInterface::class);

    $form_state = new FormState();
    $form_object = new BfmCopyFiles($this->getStringTranslationStub(), $file_system_mock, $bfm_copy_batch_mock);
    $form = $form_object->buildForm([], $form_state);

    $this->assertIsArray($form);

  }

  /**
   * Coverage test for validating form.
   */
  public function testValidateForm() {

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $file_system_mock
      ->method('prepareDirectory')
      ->willReturnOnConsecutiveCalls(TRUE, FALSE);

    $bfm_copy_batch_mock = $this->createMock(BfmCopyBatchInterface::class);

    $form_state = new FormState();
    $form_object = new BfmCopyFiles($this->getStringTranslationStub(), $file_system_mock, $bfm_copy_batch_mock);
    $form = $form_object->buildForm([], $form_state);

    $form_state->setValues(
      [
        'source' => '/tmp/invalid',
        'destination' => '/tmp/',
      ],
    );
    $form_object->validateForm($form, $form_state);
    $this->assertCount(1, $form_state->getErrors());

    $form_state = new FormState();
    $form_state->setValues(
      [
        'source' => '/tmp/',
        'destination' => '/tmp/invalid',
      ],
    );
    $form_object->validateForm($form, $form_state);
    $this->assertCount(1, $form_state->getErrors());

  }

  /**
   * Coverage test for the drush commands.
   *
   * @dataProvider providerSubmitForm
   */
  public function testSubmitForm(array $form_values, array $expected) {

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $bfm_copy_batch_mock = $this->createMock(BfmCopyBatchInterface::class);

    $form_state = new FormState();
    $form_state->setValues($form_values);

    $form_object = new BfmCopyFiles($this->getStringTranslationStub(), $file_system_mock, $bfm_copy_batch_mock);

    $form_values = array_merge(
      $form_values,
      [
        'source' => 'public://',
        'destination' => 'private://',
      ]
    );

    $expected_call = $this->logicalAnd(
      $this->arrayHasKey('upload_conditions'),
      new TraversableContainsEqual($expected)
    );

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $bfm_copy_batch_mock = $this->createMock(BfmCopyBatchInterface::class);

    $bfm_copy_batch_mock
      ->expects($this->once())
      ->method('createBatch')
      ->with('public://', 'private://', $expected_call);

    $form_state = new FormState();
    $form_state->setValues($form_values);

    $form_object = new BfmCopyFiles($this->getStringTranslationStub(), $file_system_mock, $bfm_copy_batch_mock);

    $form = $form_object->buildForm([], $form_state);
    $form_object->submitForm($form, $form_state);

  }

  /**
   * Provide data for testSubmitForm.
   *
   * @return array[]
   *   Test result array keyed by test name
   *     - array $form_values - Values to merge into $form_values.
   *     - array $expected - Key/Value pairs to look for.
   */
  public function providerSubmitForm(): array {
    return [
      'Always Upload' => [
        [
          'upload_condition' => 'always',
        ],
        [
          'always' => TRUE,
        ],
      ],
      'Upload newer or size differs' => [
        [
          'upload_condition' => 'newer_size',
        ],
        [
          'newer' => TRUE,
          'size' => TRUE,
        ],
      ],
      'Upload newer ' => [
        [
          'upload_condition' => 'newer',
        ],
        [
          'newer' => TRUE,
        ],
      ],
      'Upload size differs' => [
        [
          'upload_condition' => 'size',
        ],
        [
          'size' => TRUE,
        ],
      ],
    ];
  }

}
