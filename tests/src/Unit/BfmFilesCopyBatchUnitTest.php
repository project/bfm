<?php

namespace Drupal\Tests\bfm\Unit;

use Drupal\bfm\Batch\BfmFilesCopyBatch;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Bfm Files Copy Batch Test.
 *
 * Ensure that the drush command executes when called.
 *
 * @group bfm
 *
 * @covers \Drupal\bfm\Batch\BfmFilesCopyBatch
 */
class BfmFilesCopyBatchUnitTest extends UnitTestCase {

  /**
   * Path to be used for storing source files.
   *
   * @var string
   */
  protected string $sourceDir;

  /**
   * Path to be used as destination for file copy.
   *
   * @var string
   */
  protected string $destDir;

  /**
   * Files that will be removed during test cleanup.
   *
   * @var array
   */
  protected array $filesToDelete;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $random = $this->getRandomGenerator()->word(20);
    $this->sourceDir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $random . DIRECTORY_SEPARATOR;
    $this->destDir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $random . '_dest' . DIRECTORY_SEPARATOR;
    if (!mkdir($this->sourceDir) || !mkdir($this->destDir)) {
      $this->fail('Unable to create directories for storing files');
    }

  }

  /**
   * Coverage test for running the batches.
   */
  public function testCreateBatch() {

    $this->populateSourceFolder(110);

    $messenger_mock = $this->createMock(MessengerInterface::class);
    $batch_object = new BfmFilesCopyBatch($this->getStringTranslationStub(), $messenger_mock);

    $batch = $batch_object->createBatch($this->sourceDir, '/tmp/not_used', []);

    $files_to_copy = [];
    foreach ($batch['operations'] as $operation) {
      $files_to_copy = array_merge($files_to_copy, $operation[1][0]);
    }

    $this->assertSameSize($this->filesToDelete, $files_to_copy);
    $this->assertEqualsCanonicalizing($this->filesToDelete, $files_to_copy);

  }

  /**
   * Coverage for createBatch when source is empty.
   */
  public function testCreateBatchEmptySource() {

    $this->populateSourceFolder(110);

    $messenger_mock = $this->createMock(MessengerInterface::class);
    $messenger_mock->expects($this->once())->method('addMessage');

    $batch_object = new BfmFilesCopyBatch($this->getStringTranslationStub(), $messenger_mock);

    foreach ($this->filesToDelete as $file) {
      unlink($file);
    }
    $this->filesToDelete = [];

    $batch = $batch_object->createBatch($this->sourceDir, '/tmp/not_used', []);
    $this->assertNull($batch, "The batch is empty");

  }

  /**
   * Coverage for createBatch when source is empty.
   */
  public function testCopyFilesWhenEmpty() {

    $files = $this->populateSourceFolder(5);

    $messenger_mock = $this->createMock(MessengerInterface::class);
    $messenger_mock->method('addMessage');

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $file_system_mock->method('dirname')->willReturn($this->destDir);
    $file_system_mock->method('prepareDirectory')->willReturn(TRUE);
    $file_system_mock
      ->expects($this->exactly(5))
      ->method('copy')
      ->willReturn(TRUE);

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('file_system', $file_system_mock);
    $container->set('messenger', $messenger_mock);
    \Drupal::setContainer($container);

    $context = [];
    BfmFilesCopyBatch::copyOperation($files, $this->sourceDir, $this->destDir, [], count($files), $context);
  }

  /**
   * Coverage for copyOperation for newer files only.
   */
  public function testCopyFilesNewerOnly() {

    $files = $this->populateSourceFolder(5);
    $dest_files = $this->populateDestFolder($files);

    // Make the first 3 dest files newer than source.
    for ($i = 0; $i <= 2; $i++) {
      touch($dest_files[$i], 1650000000);
    }

    $messenger_mock = $this->createMock(MessengerInterface::class);

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $file_system_mock->method('dirname')->willReturn($this->destDir);
    $file_system_mock->method('prepareDirectory')->willReturn(TRUE);
    $file_system_mock
      ->expects($this->exactly(2))
      ->method('copy')
      ->willReturn(TRUE);

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('file_system', $file_system_mock);
    $container->set('messenger', $messenger_mock);
    \Drupal::setContainer($container);

    $context = [];
    BfmFilesCopyBatch::copyOperation($files, $this->sourceDir, $this->destDir, ['upload_conditions' => ['newer' => TRUE]], count($files), $context);
  }

  /**
   * Coverage for copyOperation for size differs only.
   */
  public function testCopyFilesSizeDiffers() {

    $files = $this->populateSourceFolder(5);
    $dest_files = $this->populateDestFolder($files);

    // Make the first 3 dest files size differ from source.
    for ($i = 0; $i <= 2; $i++) {
      file_put_contents($dest_files[$i], 'extra data', FILE_APPEND);
    }

    $messenger_mock = $this->createMock(MessengerInterface::class);

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $file_system_mock->method('dirname')->willReturn($this->destDir);
    $file_system_mock->method('prepareDirectory')->willReturn(TRUE);
    $file_system_mock
      ->expects($this->exactly(3))
      ->method('copy')
      ->willReturn(TRUE);

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('file_system', $file_system_mock);
    $container->set('messenger', $messenger_mock);
    \Drupal::setContainer($container);

    $context = [];
    BfmFilesCopyBatch::copyOperation($files, $this->sourceDir, $this->destDir, ['upload_conditions' => ['size' => TRUE]], count($files), $context);
  }

  /**
   * Coverage for copyOperation for size differs only.
   */
  public function testCopyOperationErrorHandlers() {

    $files = $this->populateSourceFolder(5);

    $messenger_mock = $this->createMock(MessengerInterface::class);

    $file_system_mock = $this->createMock(FileSystemInterface::class);
    $file_system_mock->method('dirname')->willReturn($this->destDir);
    $file_system_mock->method('prepareDirectory')->willReturnOnConsecutiveCalls(TRUE, TRUE, TRUE, TRUE, FALSE);
    $file_system_mock
      ->expects($this->exactly(4))
      ->method('copy')
      ->willReturnOnConsecutiveCalls(TRUE, TRUE, TRUE, $this->throwException(new FileException()));

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('file_system', $file_system_mock);
    $container->set('messenger', $messenger_mock);
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $context = [];
    BfmFilesCopyBatch::copyOperation($files, $this->sourceDir, $this->destDir, ['upload_conditions' => ['size' => TRUE]], count($files), $context);
    $this->assertCount(2, $context['results']['errors'], 'Expected number of errors seen');

    // @todo Check the error message is added to status.
    BfmFilesCopyBatch::finished(TRUE, $context['results'], []);

  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {

    $directory_iterator = new \RecursiveDirectoryIterator(
      $this->destDir,
      \FilesystemIterator::SKIP_DOTS
    );
    $files = new \RecursiveIteratorIterator($directory_iterator);

    foreach ($files as $file) {
      $this->filesToDelete[] = $file->getPathName();
    }

    foreach ($this->filesToDelete as $file) {
      unlink($file);
    }
    rmdir($this->sourceDir);
    rmdir($this->destDir);
    parent::tearDown();
  }

  /**
   * Populate $sourceDIr with files.
   *
   * @param int $count
   *   The number of files to populate with.
   *
   * @return string[]
   *   An array containing the path for each file created.
   */
  protected function populateSourceFolder(int $count): array {

    $files = [];

    for ($i = 0; $i < $count; $i++) {
      $random = $this->getRandomGenerator()->word(20);
      $filename = $this->sourceDir . $random;
      file_put_contents($filename, 'Test data');
      touch($filename, 1600000000);
      $this->filesToDelete[] = $filename;
      $files[] = $filename;
    }

    return $files;
  }

  /**
   * Populate $destDir with files.
   *
   * @param array $files
   *   An array of file paths to copy to $destDir.
   *
   * @return string[]
   *   An array containing the path for each file created.
   */
  protected function populateDestFolder(array $files): array {

    $copied_files = [];
    foreach ($files as $file) {
      $destination = mb_ereg_replace($this->sourceDir, $this->destDir, $file);
      $copied_files[] = $destination;
      copy($file, $destination);
      touch($destination, 1500000000);
    }
    return $copied_files;
  }

}
