<?php

namespace Drupal\Tests\bfm\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Bfm Copy Form Test.
 *
 * Ensure that the copy form loads without issue.
 *
 * @group bfm
 *
 * @coversClass \Drupal\bfm\Batch\BfmFilesCopyBatch
 */
class BfmCopyFormFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'bfm',
  ];

  /**
   * Ensure the Copy Form loads without error..
   */
  public function testCopyForm() {
    $basic_user = $this->createUser();
    $this->drupalLogin($basic_user);
    $this->drupalGet('/bfm/copy');
    $this->assertSession()->statusCodeEquals(403);

    $bfm_user = $this->createUser(['access bfm']);
    $this->drupalLogin($bfm_user);
    $this->drupalGet('/bfm/copy');
    $this->assertSession()->statusCodeEquals(200);

    $source = __DIR__;
    $destination = 'public://copy_dest';
    $values = [
      'source' => $source,
      'destination' => $destination,
      'upload_condition' => 'always',
    ];

    $this->submitForm($values, 'Copy Files');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Copied files from $source to $destination");
    $this->assertEquals(count(scandir(__DIR__)), count(scandir('public://copy_dest')));

  }

}
