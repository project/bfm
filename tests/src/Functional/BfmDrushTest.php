<?php

namespace Drupal\Tests\bfm\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drush\TestTraits\DrushTestTrait;

/**
 * Bfm Drush Test.
 *
 * Ensure that the drush command executes when called..
 *
 * @group bfm
 *
 * @coversNothing
 */
class BfmDrushTest extends BrowserTestBase {

  use DrushTestTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'bfm',
  ];

  /**
   * Coverage test for the drush commands.
   */
  public function testDrushCommands() {
    $test_files_text = $this->getTestFiles('text');

    $this->drush(
      'bfm:copy',
      [],
      [
        'source' => 'public://',
        'destination' => 'private://',
      ]
    );
    $messages = $this->getErrorOutput();
    $this->assertStringContainsString('[notice] You are going to copy files from public:// to private://.', $messages, 'Start message is sent');
    $this->assertStringContainsString('Message: Copied files from /public:/// to /private:///', $messages, "Finish message is sent");

    // Make sure we copy text files.
    /** @var \stdClass $file */
    foreach ($test_files_text as $file) {
      $src_uri = $file->uri;
      $dst_uri = mb_ereg_replace('public://', 'private://', $src_uri);
      $this->assertFileEquals($src_uri, $dst_uri, "Copied $file->uri to private://");
    }

  }

}
